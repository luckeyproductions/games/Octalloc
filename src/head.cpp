/* Octalloc
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "head.h"

Head::Head(Context* context): Controllable(context),
    rigidBody_{ nullptr }
{
}

void Head::OnNodeSet(Node* node)
{
    if (!node)
        return;

    initialSoundSources_ = 3;

    Controllable::OnNodeSet(node);

    node_->Rotate(Quaternion(Random(-23.5f, 23.5f), Vector3::FORWARD));

    //Create graphics components
    StaticModel* head{ graphicsNode_->CreateComponent<StaticModel>() };
    head->SetModel(RES(Model, "Models/MaleHead.mdl"));
    head->SetCastShadows(true);
    head->ApplyMaterialList();

    StaticModel* hat{ graphicsNode_->CreateComponent<StaticModel>() };
    hat->SetModel(RoundToInt(randomizer_ * 5.0f) % 2 ? RES(Model, "Models/JesterHat.mdl")
                                                     : RES(Model, "Models/WitchHat.mdl"));
    hat->SetCastShadows(true);
    hat->ApplyMaterialList();

    Node* weaponNode_{ graphicsNode_->CreateChild("Weapon") };
    weaponNode_->SetPosition(Vector3(1.2f, -0.8f, 2.0f));
    StaticModel* weapon{ weaponNode_->CreateComponent<StaticModel>() };
    weapon->SetModel(RES(Model, "Models/Slingshot.mdl"));
    weapon->SetCastShadows(true);
    weapon->ApplyMaterialList();

    //Set up physics components
    rigidBody_ = node_->CreateComponent<RigidBody>();
    rigidBody_->SetMass(3.0f);
    rigidBody_->SetFriction(0.05f);
    rigidBody_->SetLinearDamping(0.95f);
    rigidBody_->SetAngularDamping(0.98f);

    CollisionShape* collider{ node_->CreateComponent<CollisionShape>() };
    collider->SetSphere(2.0f, Vector3::ZERO);
}

void Head::FixedUpdate(float timeStep)
{
//    node_->Rotate(Quaternion((randomizer_ > 0.5f ? 1.0f : -1.0f) * timeStep * -23.0f, Vector3::UP), TS_LOCAL);
//    graphicsNode_->Rotate(Quaternion(timeStep * LucKey::Sine(TIME->GetElapsedTime() * 5.0f) * 42.0f, Vector3::RIGHT), TS_LOCAL);
//    node_->Translate(Vector3::FORWARD * timeStep);

    if (IsServerObject()) {
        const Controls& controls{ GetControls() };
        bool run{ controls.buttons_ & RUN };

        if (controls.buttons_ & MOVE_RIGHT)   rigidBody_->ApplyForce( node_->GetRight()     * (timeStep * 3423.0f + 42.0f * run));
        if (controls.buttons_ & MOVE_LEFT)    rigidBody_->ApplyForce(-node_->GetRight()     * (timeStep * 3423.0f + 42.0f * run));
        if (controls.buttons_ & MOVE_UP)      rigidBody_->ApplyForce( node_->GetUp()        * (timeStep * 3423.0f + 42.0f * run));
        if (controls.buttons_ & MOVE_DOWN)    rigidBody_->ApplyForce(-node_->GetUp()        * (timeStep * 3423.0f + 42.0f * run));
        if (controls.buttons_ & MOVE_FORWARD) rigidBody_->ApplyForce( node_->GetDirection() * (timeStep * 3423.0f + 42.0f * run));
        if (controls.buttons_ & MOVE_BACK)    rigidBody_->ApplyForce(-node_->GetDirection() * (timeStep * 3423.0f + 42.0f * run));

        if (controls.buttons_ & TURN_RIGHT)   rigidBody_->ApplyTorque((23.0f * run + 666.0f * timeStep) * node_->GetUp());
        if (controls.buttons_ & TURN_LEFT)    rigidBody_->ApplyTorque((23.0f * run + 666.0f * timeStep) * -node_->GetUp());
        if (controls.buttons_ & TURN_UP)      rigidBody_->ApplyTorque((23.0f * run + 666.0f * timeStep) * node_->GetRight());
        if (controls.buttons_ & TURN_DOWN)    rigidBody_->ApplyTorque((23.0f * run + 666.0f * timeStep) * -node_->GetRight());
        if (controls.buttons_ & TURN_CW)      rigidBody_->ApplyTorque((23.0f * run + 666.0f * timeStep) * -node_->GetDirection());
        if (controls.buttons_ & TURN_CCW)     rigidBody_->ApplyTorque((23.0f * run + 666.0f * timeStep) * node_->GetDirection());
        //            if (controls.buttons_ & MOVE_X) headNode->Rotate(Quaternion(timeStep * turnZ, Vector3::FORWARD));
    }

}



