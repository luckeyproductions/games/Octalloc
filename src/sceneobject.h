/* Octalloc
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include "mastercontrol.h"
#include "spawnmaster.h"

class SceneObject : public LogicComponent
{
    DRY_OBJECT(SceneObject, Object);

public:
    SceneObject(Context* context);

    virtual void Set(Vector3 position);

    Vector3 GetWorldPosition() const;
    bool IsServerObject() const;

protected:
    void OnNodeSet(Node* node) override;

    void Disable();
    void PlaySample(Sound* sample, float gain = .3f);

    unsigned initialSoundSources_;
    Node* graphicsNode_;
    float randomizer_;

private:
    void AddSoundSource();

    Vector<SoundSource3D*> sampleSources_;
};

#endif // SCENEOBJECT_H

