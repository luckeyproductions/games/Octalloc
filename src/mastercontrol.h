/* Octalloc
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include "luckey.h"

#define MC GetSubsystem<MasterControl>()

namespace Dry {
class Node;
class Scene;
}

static const String tracks[2]
{
    "Music/Centurion of War - Guitarist 2.2.ogg",
    "Music/Kevin_MacLeod_-_Master_of_the_Feast.ogg"
};
enum SoundTrack{ TRACK_MENU = 0, TRACK_PLAY };

class Player;

class MasterControl: public Application
{
    DRY_OBJECT(MasterControl, Application);

public:
    MasterControl(Context* context);

    Scene* GetScene() const { return scene_; }
    Scene* GetServerScene() const;

    void AddPlayer();
    Player* GetPlayer(int playerId) const;
    Player* GetNearestPlayer(Vector3 pos);
    Vector< SharedPtr<Player> > GetPlayers();
    void RemovePlayer(Player *player);
    void PlayMusic(SoundTrack track);

    void Setup() override;
    void Start() override;
    void Stop() override;

    void Exit();
    Scene* NewScene();

    template <class T> void RegisterObject() { context_->RegisterFactory<T>(); }
    template <class T> void RegisterSubsystem() { context_->RegisterSubsystem<T>(); }
    void HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData);

private:
    void CreateInstructions();
    void CreateScene();
    void CreateCameraAndViewport();

    Scene* scene_;
    SoundSource* musicSource_;
    Vector< SharedPtr<Player> > players_;
};

#endif // MASTERCONTROL_H
