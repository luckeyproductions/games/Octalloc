/* Octalloc
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "spawnmaster.h"
#include "barrel.h"
#include "lantern.h"

#include "realm.h"

Realm::Realm(Context* context) : SceneObject(context)
{
}

void Realm::OnNodeSet(Node *node)
{
    if (!node || !IsServerObject())
        return;

    for (int b{ 0 }; b < 5; ++b)
    {
        Node* barrelNode{ node_->CreateChild("Barrel") };
        barrelNode->CreateComponent<Barrel>();
        barrelNode->Translate(Vector3(Random(-10.0f, 10.0f),
                                      Random(-10.0f, 10.0f),
                                      Random(-10.0f, 10.0f)));
        barrelNode->Rotate(Quaternion(Random(360.0f), Random(360.0f), Random(360.0f)));
    }

    Generate();
}

void Realm::Generate()
{
    HashSet<IntVector3> emptySpace{};

    //Create room
    IntVector3 roomCenter{ IntVector3::ZERO };
//    IntVector3 roomSize{ IntVector3::ZERO };
    int width { 2u };
    int height{ 3u };
    int depth { 5u };

    for (int i{ 0 }; i < width;  ++i)
    for (int j{ 0 }; j < height; ++j)
    for (int k{ 0 }; k < depth;  ++k)
    {

        emptySpace.Insert(roomCenter + IntVector3{ i - (width  / 2),
                                                   j - (height / 2),
                                                   k - (depth  / 2) });
    }

    //Create walls
    for (const IntVector3& coords: emptySpace)
    {

        IntVector3 offset{};
        bool hasLight{ false };

        for (int d{0}; d < 6; ++d) {

            switch (d) {
            case 0: offset = IntVector3::UP;
                break;
            case 1: offset = IntVector3::DOWN;
                break;
            case 2: offset = IntVector3::LEFT;
                break;
            case 3: offset = IntVector3::RIGHT;
                break;
            case 4: offset = IntVector3::FORWARD;
                break;
            case 5: offset = IntVector3::BACK;
                break;
            default:
                break;
            }

            if (!emptySpace.Contains(coords + offset))
            {
                Vector3 center{ coords * 10.0f };
                Vector3 blockOffset{ offset * 10.0f };

                Node* wallNode{ node_->CreateChild("Block") };
                wallNode->SetPosition(center + blockOffset);
                StaticModel* wallModel{ wallNode->CreateChild()->CreateComponent<StaticModel>() };
                wallModel->SetModel(RES(Model, "Models/Box.mdl"));
                wallModel->SetMaterial(RES(Material, "Materials/Pavement.xml"));
                wallModel->GetNode()->SetScale(10.0f);

                RigidBody* wallBody{ wallNode->CreateComponent<RigidBody>() };
                wallBody->SetMass(0.0f);

                CollisionShape* collider{ wallNode->CreateComponent<CollisionShape>() };
                collider->SetBox(Vector3::ONE * 10.0f);

                if (!hasLight && !Random(5)) {

                    Vector3 lanternOffset{ offset * 5.0f };
                    Lantern* lantern{ node_->CreateChild("Lantern")->CreateComponent<Lantern>() };
                    lantern->Set(center + lanternOffset);
                    lantern->GetNode()->LookAt(center,
                                               (d > 1 ? node_->GetUp() : node_->GetRight())
                                               .CrossProduct(node_->LocalToWorld(Vector3(offset))),
                                               TS_PARENT);
                    hasLight = true;
                }
            }
        }
    }
}
