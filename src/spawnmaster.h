/* Octalloc
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef SPAWNMASTER_H
#define SPAWNMASTER_H

#include "mastercontrol.h"

class SpawnMaster: public Object
{
    friend class MasterControl;
    DRY_OBJECT(SpawnMaster, Object);

public:
    SpawnMaster(Context* context);

    void Clear();

    template <class T> T* Create(CreateMode mode = REPLICATED)
    {
        MasterControl* mc{ GetSubsystem<MasterControl>() };
        Scene* scene{ mc->GetScene() };

        if (mode == REPLICATED && GetSubsystem<Network>()->IsServerRunning())
            scene = mc->GetServerScene();

        Node* spawnedNode{ scene->CreateChild(T::GetTypeStatic().ToString(), mode) };
        T* created{ spawnedNode->CreateComponent<T>() };

        return created;
    }

    template <class T> int CountActive()
    {
        MasterControl* mc{ GetSubsystem<MasterControl>() };

        int count{0};
        PODVector<Node*> result{};
        mc->GetScene()->GetChildrenWithComponent<T>(result);

        for (Node* r : result) {

            if (r->IsEnabled())
                ++count;
        }
        return count;
    }

private:
    void Activate();
    void Deactivate();
    void Restart();

    void HandleSceneUpdate(StringHash eventType, VariantMap &eventData);
};

#endif // SPAWNMASTER_H
