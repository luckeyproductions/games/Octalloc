/* Octalloc
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "barrel.h"

Barrel::Barrel(Context* context): SceneObject(context)
{
}

void Barrel::OnNodeSet(Node* node)
{
    if (!node)
        return;

    SceneObject::OnNodeSet(node);

    StaticModel* model{ graphicsNode_->CreateComponent<StaticModel>() };
    model->SetModel(RES(Model, "Models/Barrel.mdl"));
    model->ApplyMaterialList();

    RigidBody* body{ node_->CreateComponent<RigidBody>() };
    body->SetMass(2.0f);
    body->SetLinearDamping(0.75f);
    body->SetAngularDamping(0.23f);
    body->SetFriction(0.23f);
    body->SetLinearRestThreshold(0.023f);
    body->SetAngularRestThreshold(0.042f);

    CollisionShape* collider{ node_->CreateComponent<CollisionShape>() };
    collider->SetSphere(2.3f);
}

void Barrel::Update(float timeStep)
{
}



