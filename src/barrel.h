/* Octalloc
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BARREL_H
#define BARREL_H

#include "sceneobject.h"

class Barrel : public SceneObject
{
    DRY_OBJECT(Barrel, SceneObject);
public:
    static void RegisterObject(Context* context);

    Barrel(Context* context);
    void Update(float timeStep) override;

protected:
    void OnNodeSet(Node* node) override;
};

#endif // BARREL_H
