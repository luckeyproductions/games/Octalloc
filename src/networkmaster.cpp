/* Octalloc
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mastercontrol.h"
#include "spawnmaster.h"
#include "inputmaster.h"

#include "realm.h"
#include "head.h"
#include "lantern.h"

#include "networkmaster.h"

NetworkMaster::NetworkMaster(Context* context): Object(context),
    clientObjectID_{},
    serverObjects_{},
    serverScene_{ nullptr }
{
    SubscribeToEvent(E_KEYDOWN, DRY_HANDLER(NetworkMaster, HandleKeyDown));
    SubscribeToEvent(E_CLIENTCONNECTED, DRY_HANDLER(NetworkMaster, HandleClientConnected));
    SubscribeToEvent(E_CLIENTOBJECTID, DRY_HANDLER(NetworkMaster, HandleClientObjectID));
    SubscribeToEvent(E_CLIENTDISCONNECTED, DRY_HANDLER(NetworkMaster, HandleClientDisconnected));

    SubscribeToEvent(E_PHYSICSPRESTEP, DRY_HANDLER(NetworkMaster, HandlePhysicsPreStep));
    SubscribeToEvent(E_POSTUPDATE, DRY_HANDLER(NetworkMaster, HandlePostUpdate));

    GetSubsystem<Network>()->RegisterRemoteEvent(E_CLIENTOBJECTID);
}

void NetworkMaster::HandleKeyDown(StringHash eventType, VariantMap& eventData)
{
    MasterControl* mc{ GetSubsystem<MasterControl>() };
    Network* network{ GetSubsystem<Network>() };
    UI* ui{ GetSubsystem<UI>() };

    bool isServer{ network->IsServerRunning() };
    bool isClient{ network->GetServerConnection() != nullptr };

    int key{ eventData[KeyDown::P_KEY].GetInt() };


    if (!isClient && !isServer && key == KEY_H) {

        CreateServerScene();
        network->StartServer(SERVER_PORT);
        ui->GetRoot()->RemoveAllChildren();

    } else if (!isClient && key == KEY_J) {

        network->Connect("localhost", SERVER_PORT, mc->GetScene());
        ui->GetRoot()->RemoveAllChildren();
        MC->PlayMusic(TRACK_PLAY);
    }
}

void NetworkMaster::HandleClientConnected(StringHash eventType, VariantMap& eventData)
{ using namespace ClientConnected;

    SpawnMaster* spawn{ GetSubsystem<SpawnMaster>() };

    Connection* newConnection{ static_cast<Connection*>(eventData[P_CONNECTION].GetPtr()) };
    newConnection->SetScene(GetServerScene());

    Head* head{ spawn->Create<Head>() };
    serverObjects_[newConnection] = head;

    VariantMap remoteEventData{};
    remoteEventData[P_ID] = head->GetNode()->GetID();
    newConnection->SendRemoteEvent(E_CLIENTOBJECTID, true, remoteEventData);
}
void NetworkMaster::HandleClientObjectID(StringHash eventType, VariantMap& eventData)
{
    MasterControl* mc{ GetSubsystem<MasterControl>() };

    clientObjectID_ = eventData[P_ID].GetUInt();
}

void NetworkMaster::HandleClientDisconnected(StringHash eventType, VariantMap& eventData)
{ using namespace ClientDisconnected;

    // When a client disconnects, remove the controlled object
    Connection* connection{ static_cast<Connection*>(eventData[P_CONNECTION].GetPtr()) };
    Node* node{ serverObjects_[connection]->GetNode() };

    if (node)
        node->Remove();

    serverObjects_.Erase(connection);
}
Scene* NetworkMaster::CreateServerScene()
{
    if (serverScene_)

        serverScene_->Clear();

    else {

        serverScene_ = GetSubsystem<MasterControl>()->NewScene();

    }

    serverScene_->CreateComponent<Realm>();

    return serverScene_;
}

void NetworkMaster::HandlePhysicsPreStep(StringHash eventType, VariantMap& eventData)
{ using namespace PhysicsPreStep;

    Network* network{ GetSubsystem<Network>() };
    Connection* serverConnection{ network->GetServerConnection() };
    Input* input{ GetSubsystem<Input>() };

    if (serverConnection) {

        Controls controls{}; ///Should be gathered by InputMaster for keybinding and more players per machine.

        controls.Set(MOVE_RIGHT,   input->GetKeyDown(KEY_D));
        controls.Set(MOVE_LEFT,    input->GetKeyDown(KEY_A));
        controls.Set(MOVE_UP,      input->GetKeyDown(KEY_Q));
        controls.Set(MOVE_DOWN,    input->GetKeyDown(KEY_E));
        controls.Set(MOVE_FORWARD, input->GetKeyDown(KEY_W));
        controls.Set(MOVE_BACK,    input->GetKeyDown(KEY_S));
        controls.Set(TURN_RIGHT,   input->GetKeyDown(KEY_KP_6));
        controls.Set(TURN_LEFT,    input->GetKeyDown(KEY_KP_4));
        controls.Set(TURN_UP,      input->GetKeyDown(KEY_KP_2) || input->GetKeyDown(KEY_KP_5));
        controls.Set(TURN_DOWN,    input->GetKeyDown(KEY_KP_8));
        controls.Set(TURN_CW,      input->GetKeyDown(KEY_KP_3) || input->GetKeyDown(KEY_KP_9));
        controls.Set(TURN_CCW,     input->GetKeyDown(KEY_KP_1) || input->GetKeyDown(KEY_KP_7));
        controls.Set(RUN,          input->GetKeyDown(KEY_SHIFT) || input->GetKeyDown(KEY_RSHIFT));

        serverConnection->SetControls(controls);
//        serverConnection->SetPosition(cameraNode_->GetPosition());

    }
    if (network->IsServerRunning()) {

        for (Connection* connection: network->GetClientConnections()) {

            serverObjects_[connection]->SetControls(connection->GetControls());

        }
    }
}

void NetworkMaster::HandlePostUpdate(StringHash eventType, VariantMap& eventData)
{
    Scene* scene{ GetSubsystem<MasterControl>()->GetScene() };
    Node* clientNode{ scene->GetNode(clientObjectID_) };
    Node* cameraNode{ scene->GetChild("Camera") };

    //Reassign camera node
    if (clientNode && cameraNode && cameraNode->GetParent() != clientNode) {

        cameraNode->SetParent(clientNode);
        cameraNode->SetPosition(Vector3::BACK * 0.34f);
        cameraNode->SetRotation(Quaternion::IDENTITY);

//        clientNode->GetChild("Graphics")->SetEnabled(false);
    }
}
