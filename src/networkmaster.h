/* Octalloc
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef NETWORKMASTER_H
#define NETWORKMASTER_H

#include "luckey.h"

static const unsigned short SERVER_PORT = 2342;
static const StringHash E_CLIENTOBJECTID{"ClientObjectID"};
static const StringHash P_ID{"ID"};

class Controllable;

class NetworkMaster: public Object
{
    DRY_OBJECT(NetworkMaster, Object);

public:
    NetworkMaster(Context* context);

    void HandleKeyDown(StringHash eventType, VariantMap& eventData);
    void HandleClientConnected(StringHash eventType, VariantMap& eventData);
    void HandleClientObjectID(StringHash eventType, VariantMap& eventData);
    void HandleClientDisconnected(StringHash eventType, VariantMap& eventData);
    void HandlePhysicsPreStep(StringHash eventType, VariantMap& eventData);
    void HandlePostUpdate(StringHash eventType, VariantMap& eventData);

    Scene* GetServerScene() const { return serverScene_; }

private:
    unsigned clientObjectID_;
    HashMap<Connection*, Controllable*> serverObjects_;
    Scene* serverScene_;

    Scene* CreateServerScene();
};

#endif // NETWORKMASTER_H
