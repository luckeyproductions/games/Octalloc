# Octalloc

Beyond the chopping block lies the Realm of Octalloc.


 Keys   | Action
--------|---------
 WASDQE | Move
 Numpad | Rotate

![Jester](Docs/jester.png)
