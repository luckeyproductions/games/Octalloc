### Characters

- Knight - Sword
- Dwarf - Axe
- Jester - Slingshot
- Witch - Zap wand
- Faerie - Ice darts
- Wizard - Fire staff
- Archer - Bow
- Crossbowman - Crossbow
- Ninja - Shuriken


### Game modes

- Deathmatch
- Capture the banner
- Save the princess


### Lobby

Character joins round table after being picked by the player.
The table is divided in colours if there are teams.
Signaling ready is displayed as raising one's receptacle. After a while it will be lowered again. Repeatedly signaling ready in a short time shows impatience/aggravation through animation.


## Areas/Themes

- Castle/Cathedral
- Mines/Cave
- Temple

The Avatars that the Players controll were those that fel in their brave attempts to obstruct the Coronation of [Setagllib](http://www.azillionmonkeys.com/qed/dosalot1.html).
