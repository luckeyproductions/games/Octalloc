include(src/Octalloc.pri)

TARGET = octalloc

LIBS += \
    ../Octalloc/Dry/lib/libDry.a \
    -lpthread \
    -ldl \
    -lGL

QMAKE_CXXFLAGS += -std=c++11 -O2

INCLUDEPATH += \
    ../Octalloc/Dry/include \
    ../Octalloc/Dry/include/Dry/ThirdParty \

TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt

DISTFILES += \
    LICENSE_TEMPLATE
